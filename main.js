//1//////////////////////////////////////////////////////
var itemOne = [1,2,4,3]

function findHighestValue(parameter)
{
    var highestValue;
    parameter.sort();
    
    highestValue = parameter[(parameter.length - 1)];

    return highestValue;
}

console.log (findHighestValue(itemOne));

//2//////////////////////////////////////////////////////

var itemTwo = [1,2,3,4]

function sumOfArray(parameter)
{
    var sum = 0;
    parameter.forEach(element => 
    {
        sum += element;
    });

    return sum;
}

console.log (sumOfArray(itemTwo));

//3//////////////////////////////////////////////////////

var itemThree = [1,2,3,4]

function averageOfArray(parameter)
{
    var average = 0;
    parameter.forEach(element => 
    {
        average += element;
    });

    average /= parameter.length;

    return average;
}

console.log(averageOfArray(itemThree));

//4//////////////////////////////////////////////////////

var itemFour = 150

function degreeToRadians(parameter)
{
    var radians = 3.14/180;
    
    radians *= parameter;

    return radians;
}

console.log(degreeToRadians(itemFour));

//5//////////////////////////////////////////////////////

var itemFive = 5;
var maxValue = 3;
var minValue = 1;

function clampToMax(parameter)
{
    if(itemFive > maxValue)
    {
        itemFive = maxValue;
    }
}

console.log(itemFive);

itemFive = 0;

function clampToMin(parameter)
{
    if(itemFive < minValue)
    {
        itemFive = minValue;
    }
}

console.log(itemFive);

//6//////////////////////////////////////////////////////

var itemSixA = [5,5];
var itemSixB = [1,1];
function getDistance(parameter0, parameter1)
{
    var distance;
    let i = parameter0[0] - parameter1[0];
    let j = parameter0[1] - parameter1[1];

    i *= i;
    j *= j;

    distance = Math.sqrt((i + j));

    return distance;
}

console.log(getDistance(itemSixA, itemSixB));

//7//////////////////////////////////////////////////////

var itemSeven = [1,4,5,6]

function sortArray(parameter)
{
    parameter.sort();
}

//8//////////////////////////////////////////////////////

var itemEight = new Object;
itemEight = 
{
    X: 3,
    Y: 4,
    Magnitude: function()
    {
        return 5;
    }
}

//9//////////////////////////////////////////////////////

var First = "Michael";
var Last = "Rivera";

var itemNine;

class Person
{
    constructor(parameter0, parameter1)
    {
        this.FirstName = parameter0;
        this.LastName = parameter1;
    }

    getFullName() 
    {
        return this.FirstName + " " + this.LastName;
    }
}

itemNine = new Person(First, Last);
console.log(itemNine.getFullName());

//10/////////////////////////////////////////////////////

